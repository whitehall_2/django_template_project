 .. _users:

White2
======================================================================

This is the root app for the Whitehall II Django project, it contains the participants and current information about them.

.. automodule:: white2_project.white2.models
   :members:
   :noindex:

