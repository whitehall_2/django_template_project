# A template for DSH Django project, particularly for Whitehall II

## White2

Changed the structure to one database but decided not to use generic foreign keys





[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

License: MIT

## Settings

Moved to [settings](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

## Basic Commands

## Environment

Before running anything with manage.py you need to have 3 things in place:
-   Conda environment (python 3.9)

        $ conda create --name env1 python=3.9

    , but use pip to install requirements, e.g. for local:
    
        $ pip install -r requirements/local.txt

-   The .env file in the repo root (make a copy of .env_template and replace the bits)
-   Set an environment variable 

    $ Set DJANGO_READ_DOT_ENV_FILE=True

### Setting Up Your Users

The following is standard setup, but we'll be adding LDAP, so this will change
>>>
-   To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

-   To create a **superuser account**, use this command:

        $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.
>>>
### Type checks

Running type checks with mypy:

    $ mypy white2_project

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report:

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

#### Running tests with pytest

    $ pytest

### Live reloading and Sass CSS compilation

Moved to [Live reloading and SASS compilation](https://cookiecutter-django.readthedocs.io/en/latest/developing-locally.html#sass-compilation-live-reloading).

### Deployment

