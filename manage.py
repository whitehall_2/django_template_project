#!/usr/bin/env python
import os
import sys
from pathlib import Path

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")

    #this is to set the default port to 8080, to run the production server "python manage.py runserver 8000"
#    from django.core.management.commands.runserver import Command as runserver
#    runserver.default_port = "8080"
    
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django  # noqa
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )

        raise

    # This allows easy placement of apps within the interior
    # white2_project directory.
    current_path = Path(__file__).parent.resolve()
    sys.path.append(str(current_path / "white2_project"))

    execute_from_command_line(sys.argv)
