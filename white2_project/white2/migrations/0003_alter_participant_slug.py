# Generated by Django 3.2.12 on 2022-05-25 08:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('white2', '0002_auto_20220525_0921'),
    ]

    operations = [
        migrations.AlterField(
            model_name='participant',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
