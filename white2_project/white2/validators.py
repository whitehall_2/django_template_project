from django.core.exceptions import ValidationError
from datetime import date

def date_of_birth_in_range(value):
    if value < date(1930,5,1) or value > date(1952,11,28):
        msg = 'Date of birth is out of range for a W2 participant.'
        raise ValidationError(msg)
 
    