from django.urls import path

from .views import ParticipantListView, ParticipantDetailView ,ParticipantUpdateView,ParticipantContactUpdateView

app_name = "white2"
urlpatterns = [
    path('', view=ParticipantListView.as_view(), name="participants"),
    path('<slug:slug>/update/', view=ParticipantUpdateView.as_view(), name="participant-update"),
    path('<slug:slug>/contact/', view=ParticipantContactUpdateView.as_view(), name="participant-contact-update"),
    path('<slug:slug>/', view=ParticipantDetailView.as_view(), name="participant"),
    
] 