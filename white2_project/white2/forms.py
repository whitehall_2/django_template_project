from django import forms
from django.forms import inlineformset_factory
from django.urls import reverse
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit,Layout,Fieldset,ButtonHolder,Row,Column,Field
from crispy_forms.bootstrap import InlineField

from .models import Participant, ParticipantAddress, ParticipantPhone, ParticipantEmail



class ParticipantAddressForm(forms.ModelForm):
    '''Model form for participant address, using crispy'''
    class Meta:
        model=ParticipantAddress
        exclude = ()

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-participantaddressform'
        self.helper.form_class = 'form-horizontal'
        self.helper.field_class = 'col-lg-11'
        self.helper.add_input(Submit("submit", "Save address changes"))
        self.helper.render_hidden_fields=True
        self.helper.layout = Layout(
            Row(
                Column('primary',css_class='form-group col-md-1 mb-0'),
                Column('carehome',css_class='form-group col-md-2 mb-0'),
                Column('location',css_class='form-group col-md-3 mb-0'),
                css_class='form-row'
            ),
            Row('address_line_1',css_class='form-group col-md-6 mb-0'),
            Row('address_line_2',css_class='form-group col-md-6 mb-0'),
            Row('address_line_3',css_class='form-group col-md-6 mb-0'),
            Row('address_line_4',css_class='form-group col-md-6 mb-0'),
            Row('address_line_5',css_class='form-group col-md-6 mb-0'),
            Row('address_line_6',css_class='form-group col-md-6 mb-0'),
            Row('country',css_class='form-group col-md-4 mb-0'),
            Row('postcode',css_class='form-group col-md-4 mb-0'),
                    
            
        )
        return super(ParticipantAddressForm, self).__init__(*args, **kwargs)

'''
A formset for address, has to be built using 2 models to deal with FK
'''
AddressFormset = inlineformset_factory(
    Participant,
    ParticipantAddress,
    form=ParticipantAddressForm,
    extra = 0
)

class ParticipantPhoneForm(forms.ModelForm):
    '''form for phone using crispy forms'''
    class Meta:
        model = ParticipantPhone
        fields = ['number','type','location','primary']
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-participantphoneform'
        self.helper.form_class = 'form-horizontal'
        self.helper.render_hidden_fields=True
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Row(
                Column('primary',css_class='form-group col-md-1 mb-0'),
                Column('number',css_class='form-group col-md-3 mb-0'),
                Column('type',css_class='form-group col-md-2 mb-0'),
                Column('location',css_class='form-group col-md-2 mb-0'),
                Column(Submit('save','Save phone number',css_class='button white')),
                css_class='form-row'
            )
            
        )
        self.helper.field_class = 'col-lg-11'

        return super(ParticipantPhoneForm, self).__init__(*args, **kwargs)

'''
A formset for phone, has to be built using 2 models to deal with FK
'''
PhoneFormset = inlineformset_factory(
    Participant,
    ParticipantPhone,
    form=ParticipantPhoneForm,
    extra = 0
)
        
class ParticipantEmailForm(forms.ModelForm):
    '''form for email using crispy forms'''
    class Meta:
        model=ParticipantEmail
        fields = ['email','location','primary']
        

    def __init__(self, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-participantemailform'
        self.helper.form_class = 'form-horizontal'
        self.helper.render_hidden_fields=True

        self.helper.layout = Layout(
            Row(
                Column('primary',css_class='form-group col-md-1 mb-0'),
                Column('email',css_class='form-group col-md-3 mb-0'),
                Column('location',css_class='form-group col-md-2 mb-0'),
                Column(Submit('save','Save email',css_class='button white')),
            ),
            
        )
        self.helper.field_class = 'col-lg-11'

        return super(ParticipantEmailForm, self).__init__(*args, **kwargs)

'''
A formset for email, has to be built using 2 models to deal with FK
'''
EmailFormset = inlineformset_factory(
    Participant,
    ParticipantEmail,
    form=ParticipantEmailForm,
    extra = 0
)


class ParticipantForm(forms.ModelForm):
    '''form for participant, using crispy forms'''
    class Meta:
        model = Participant
        fields = [
            'study_no',
            'check_letter',
            'surname',
            'first_name',
            'known_as',
            'initials',
            'title',
            'previous_surname',
            'date_of_birth',
            'sex',
            'sts_dead',
            'sts_withdrawn',
            'sts_overseas',
            'sts_active',
            'sts_prefers_home_visit',
            'sts_questionnaire',
            'sts_screening',
            'sts_phone',
            'sts_email',
            'sts_telephone_interview',
            'sts_tracing_possibly_lost',
            'sts_new_address_found',
            'slug',
        ]
        
        
    def __init__(self, *args, **kwargs):
        #self.user = kwargs.pop('user')
        super(ParticipantForm,self).__init__(*args,**kwargs)
        
        self.helper = FormHelper()
        self.helper.form_id = 'id-participantform'
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-11'
        self.helper.field_class = 'col-lg-11'
        self.helper.render_hidden_fields=True

        self.helper.layout = Layout(
            Row(
                Fieldset(
                    "Study number", 
                    InlineField('study_no',readonly=True),
                    InlineField('check_letter',readonly=True),
                    Field('slug',type="hidden")
                ),
            ),
            Row(
                Column(
                    Fieldset(
                        "Participant:",
                        'surname',
                        'first_name',
                        'known_as',
                        'initials',
                        'title',
                        'previous_surname',
                        'date_of_birth',
                        'sex',
                        
                    ),
                css_class='col-md-6 mb-0 align-items-center'
                ),
                Column(
                    Fieldset(
                        'Status:',
                        Field('sts_dead',css_class='checkbox-primary'),
                        Field('sts_withdrawn',css_class='checkbox-primary'),
                        Field('sts_overseas',css_class='checkbox-primary'),
                        Field('sts_active',css_class='form-check-input'),
                        Field('sts_prefers_home_visit',css_class='form-check-input'),
                        Field('sts_questionnaire',css_class='form-check-input'),
                        Field('sts_screening',css_class='form-check-input'),
                        Field('sts_phone',css_class='form-check-input'),
                        Field('sts_email',css_class='form-check-input'),
                        Field('sts_telephone_interview',css_class='form-check-input'),
                        Field('sts_tracing_possibly_lost',css_class='form-check-input'),
                        Field('sts_new_address_found',css_class='form-check-input'),
                    
                    ),
                css_class='col-md-3 form-check align-items-center'
                )
            ),
            Row(
                ButtonHolder(
                    Submit('save','Save changes',css_class='button white')
                )
            )
        )
    
        
