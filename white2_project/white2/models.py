
from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify
from django_countries.fields import CountryField

from .validators import date_of_birth_in_range


def get_all_instance_fields(instance):
    """Returns a list of dictionaries of all fields (label, name, attrs) on the instance.
       
    """
    fields = []
    for f in instance._meta.fields:

        fname = f.name        
        # resolve picklists/choices, with get_xyz_display() function
        get_choice = 'get_'+fname+'_display'
        if hasattr(instance, get_choice):
            value = getattr(instance, get_choice)
        else:
            try:
                value = getattr(instance, fname)
            except AttributeError:
                value = None

        # only display fields with values and skip some fields entirely
        if f.editable and (value or type(f) is models.BooleanField) and f.name not in ('id') :
            ftype = type(f).__name__
            fields.append(
            {
            'label':f.verbose_name, 
            'name':f.name, 
            'value':value,
            'ftype':ftype
            }
            )
    return fields
    
class UpperCaseCharField(models.CharField):
    '''A field type that forces itself to save in upper case'''
    def __init__(self, *args, **kwargs):
        super(UpperCaseCharField, self).__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        '''Changes field's value to uppercase and returns uppercase value before saving. 
        
        Args:
            model_instance: The instance 
        Returns: 
            str: the return value - uppercase entered value
        '''
        value = getattr(model_instance, self.attname, None)
        if value:
            value = value.upper()
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(UpperCaseCharField, self).pre_save(model_instance, add)
    
class TimestampMixin(models.Model):
    '''more of a base class really
       adds timestamps too all models that derive
    '''
    datetime_created = models.DateTimeField(auto_now_add=True)
    datetime_updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
             
'''class Country(TimestampMixin):
    code = models.CharField(max_length=4, primary_key=True)
    name = models.CharField(max_length=127)

    class Meta:
        verbose_name_plural="countries"
        
    def __str__(self):
        return u"%s (%s)"%(self.name,self.code)
'''    
       
class ContactMethod(TimestampMixin):
    '''
    abstract base class for the 3 (at the moment) contact types
    contains common fields
    '''
    class Location(models.TextChoices):
        UNSPECIFIED = "unspecified", "Unspecified"
        HOME = "home", "Home"
        WORK = "work", "Work"
        TEMPORARY = "temporary", "Temporary"
    
    location = models.CharField(max_length=20, choices=Location.choices,default=Location.UNSPECIFIED)
    primary = models.BooleanField(default=False)
    class Meta:
        abstract = True
        
class Participant(TimestampMixin):
    study_no = models.IntegerField('study number',unique=True, db_index=True)
    check_letter = UpperCaseCharField('check letter', max_length=1)
    surname = models.CharField('surname', max_length=50, db_index=True)
    first_name = models.CharField('first name', max_length=50)
    known_as = models.CharField('known as', max_length=50,blank=True,null=True)
    initials = UpperCaseCharField('initials', max_length=10)
    title = models.CharField('title', max_length=50,help_text="This is the participant's title, like Lord or Lady")
    previous_surname = models.CharField('previous surname', max_length=50,blank=True,null=True )
    date_of_birth = models.DateField('date of birth', validators=[date_of_birth_in_range])
    sex = models.IntegerField('sex', choices = [(1, 'Male'),(2, 'Female')])
#status variables
    sts_dead = models.BooleanField('dead', default = False)
    sts_withdrawn = models.BooleanField('withdrawn', default = False )
    sts_overseas = models.BooleanField('overseas', default = False )
    sts_active = models.BooleanField('active', default = False)
    sts_prefers_home_visit  = models.BooleanField('prefers HV', default = False)
    sts_questionnaire = models.BooleanField('accepts HSQ', default = False)
    sts_screening = models.BooleanField('accepts screening', default = False)
    sts_phone = models.BooleanField('accepts phone calls', default = False)
    sts_email  = models.BooleanField('accepts email', default = False)
    sts_telephone_interview = models.BooleanField('accepts telephone interview', default = False)
    sts_tracing_possibly_lost = models.BooleanField('tracing, possibly lost', default = False)
    sts_new_address_found = models.BooleanField('new address found / confirmed', default = False)
    
    slug = models.SlugField(unique=True,blank=False,db_index=True) 
    
    class Meta:
        db_table = 'participant'
        ordering = ['study_no']
        verbose_name = 'participant'
        verbose_name_plural = 'participants'
        app_label = 'white2'
        
    def __str__(self):
        return str(self.study_no) + self.check_letter.upper() + ', ' + self.title_inits_surname()
    
    def title_inits_surname(self):
        """Get display name for participant.

        Returns:
            str: display name for participant.

        """
        return "%s %s %s" % (self.title, self.initials, self.surname)
    
    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("white2:participant", kwargs={"slug": self.slug})
    
    def get_all_fields(self):
        """Returns a list of dictionaries of all fields (label, name, attrs) on the instance."""
        fields = get_all_instance_fields(self)
        return fields

    def get_non_status_fields(self):
        """Returns a list of dictionaries of all fields (label, name, attrs) on the instance
           that are not status fields
        """
        
        all_fields = self.get_all_fields()
        fields = []
        for field in all_fields:
            if field["name"][:4] != 'sts_':
                fields.append(field)
        return fields
   
    def get_status_fields(self):
        """Returns a list of dictionaries of all fields (label, name, attrs) on the instance
           that are status fields
        """
        all_fields = self.get_all_fields()
        fields = []
        for field in all_fields:
            if field["name"][:4]=='sts_':
                fields.append(field)
        return fields
    
    def save(self,*args,**kwargs):
        '''Override save to set the slug
        '''
        if not self.slug:
            self.slug = slugify(str(self.study_no) + str.upper(self.check_letter))#str.upper appears to haveno effect
        return super().save(*args,**kwargs)
        
#    def get_slug(self):
#        return slugify(self.study_no + self.check_letter)

#    def primary_address(self):
#        '''
#        get the primary address, if there is one
#        '''
#        try:
#            return self.addresses.filter(primary=True)[0]
#        except IndexError:
#            return None

#    def primary_telephone(self):
#        '''
#        get the primary telephone if there is one
#        '''
#        try:
#            return self.telephones.filter(primary=True)[0]
#        except IndexError:
#            if self.telephones.exists():
#                return self.telephones.all()[0]
#            return None
        
#    def primary_email(self):
#        '''
#        get primary email if there is one
#        '''
#        try:
#            return self.emails.filter(primary=True)[0]
#        except IndexError:
#            return None


    '''haven't included a delete method'''

class ParticipantPhone(ContactMethod):
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE, related_name='phones')
    number = models.CharField('number', max_length=50)
    type = models.CharField('type of number', max_length=10, choices=[('Mobile','Mobile'),('Landline','Landline'),('Fax','Fax')])
    
    class Meta:
        db_table = 'participant_phone'
        verbose_name = 'phone'
        verbose_name_plural = 'phone numbers'
        
#    def get_all_fields(self):
#        """Returns a list of dictionaries of all fields (label, name, attrs) on the instance."""
#        fields = get_all_instance_fields(self)
#        return fields
    
    def __str__(self):
        return self.number
    
class ParticipantEmail(ContactMethod):
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE, related_name='emails')
    email = models.EmailField('email address')
    
    class Meta:
        db_table = 'participant_email'
        verbose_name = 'email address'
        verbose_name_plural = 'email addresses'

#    def get_all_fields(self):
#        """Returns a list of dictionaries of all fields (label, name, attrs) on the instance."""
#        fields = get_all_instance_fields(self)
#        return fields
    
    def __str__(self):
        return self.email
    
    
class ParticipantAddress(ContactMethod):
    participant = models.ForeignKey(Participant, on_delete=models.CASCADE, related_name='addresses')
    address_line_1 = models.CharField('address line 1', max_length=100)
    address_line_2 = models.CharField('address line 2', max_length=100, blank = True, null = True)
    address_line_3 = models.CharField('address line 3', max_length=100, blank = True, null = True)
    address_line_4 = models.CharField('address line 4', max_length=100, blank = True, null = True)
    address_line_5 = models.CharField('address line 5', max_length=100, blank = True, null = True)
    address_line_6 = models.CharField('address line 6', max_length=100, blank = True, null = True)
    country = CountryField(blank_label='(select country)')
    postcode = UpperCaseCharField('postcode', max_length=10, db_index=True, blank = True, null = True)
    carehome = models.BooleanField('care home' , default=False)

    class Meta:
        db_table = 'participant_addresses'
        verbose_name = 'address'
        verbose_name_plural = 'addresses'
    
#    def get_all_fields(self):
#        """Returns a list of dictionaries of all fields (label, name, attrs) on the instance."""
#        fields = get_all_instance_fields(self)
#        return fields  
        
    def __str__(self): #maybe too much info?
        rv = self.address_line_1
        if self.address_line_2:
            rv += ', ' +self.address_line_2
        if self.address_line_3:
            rv += ', ' +self.address_line_3
        if self.address_line_4:
            rv += ', ' +self.address_line_4
        if self.address_line_5:
            rv += ', ' +self.address_line_5
        if self.address_line_6:
            rv += ', ' +self.address_line_6
        if self.postcode:
            rv += ', ' +self.postcode
        if self.country:
            rv += ', ' +self.country.name
        return rv
        
