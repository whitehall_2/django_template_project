import pytest
from pytest_django.asserts import assertContains
from django.urls import reverse

from white2_project.users.models import User
#from white2_project.users.tests.factories import UserFactory

from .factories import ParticipantFactory
from ..models import Participant
from ..views import ParticipantListView,ParticipantDetailView

pytestmark = pytest.mark.django_db

class TestParticipantListView:
    def test_good_participant_list_view(self,rf):
        #rf = shortcut to RequestFactory
        request = rf.get(reverse('white2:participants'))
        response = ParticipantListView.as_view()(request)
        #should return http 200 and html contain 'Participant List'
        assertContains(response,'Participant List')
        
    #this one's not working - not getting at the .head() of the response
    
#    def test_head(self, user: User, rf):
#        view = ParticipantListView()
#        request = rf.get(reverse('white2:participants'))
#        request.user = user
#        view.request = request
#        response = ParticipantListView.as_view()(request)
#        print(response.headers)
#        #head() is override, so don't need to call explicitly (I think)
#        assertContains(response,'Last-Modified')
        
        
class TestParticipantDetailView:
    def test_good_participant_detail_view(self,user:User,rf):
        participant = ParticipantFactory()
        url = reverse(
            'white2:participant',
            kwargs={'slug':participant.slug}
            )
        request = rf.get(url)
        request.user = user
        response = ParticipantDetailView.as_view()(request,slug = participant.slug)
        assertContains(response,participant.surname)
    
    