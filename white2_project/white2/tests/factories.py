import pytest
from datetime import datetime
from typing import Any, Sequence
from django.template.defaultfilters import slugify

from django.contrib.auth import get_user_model
from factory import Faker, post_generation
from factory.django import DjangoModelFactory
import factory
import factory.fuzzy

from white2_project.white2.models import Participant, ParticipantPhone, ParticipantEmail, ParticipantAddress

class ParticipantFactory(DjangoModelFactory):

    study_no = factory.fuzzy.FuzzyInteger(low=10001,high=999999)
    slug = factory.LazyAttribute(lambda obj: slugify(str(obj.study_no) + str.upper(obj.check_letter)))
    check_letter = factory.fuzzy.FuzzyText(length=1)
    surname = factory.Faker('name')# fuzzy.FuzzyText()
    first_name = factory.Faker('name')# .fuzzy.FuzzyText()
    known_as = factory.Faker('name')# fuzzy.FuzzyText()
    initials = factory.fuzzy.FuzzyText(length=3)
    title = factory.fuzzy.FuzzyChoice(['Mr.','Ms.','Mrs','Lord','Lady'])
    previous_surname = factory.Faker('name')# .fuzzy.FuzzyText()
    date_of_birth = factory.fuzzy.FuzzyDate(start_date=datetime(1945,1,1),end_date=datetime(1960,1,1))
    sex = factory.fuzzy.FuzzyChoice([1,2])
#status variables
    sts_dead = factory.fuzzy.FuzzyChoice([True,False])
    sts_withdrawn = factory.fuzzy.FuzzyChoice([True,False])
    sts_overseas = factory.fuzzy.FuzzyChoice([True,False])
    sts_active = factory.fuzzy.FuzzyChoice([True,False])
    sts_prefers_home_visit  =factory.fuzzy.FuzzyChoice([True,False])
    sts_questionnaire = factory.fuzzy.FuzzyChoice([True,False])
    sts_screening = factory.fuzzy.FuzzyChoice([True,False])
    sts_phone = factory.fuzzy.FuzzyChoice([True,False])
    sts_email  = factory.fuzzy.FuzzyChoice([True,False])
    sts_telephone_interview = factory.fuzzy.FuzzyChoice([True,False])
    sts_tracing_possibly_lost = factory.fuzzy.FuzzyChoice([True,False])
    sts_new_address_found = factory.fuzzy.FuzzyChoice([True,False])
    
    class Meta:
        model = Participant
        
        
class ParticipantPhoneFactory(DjangoModelFactory):
    participant = factory.SubFactory(
        ParticipantFactory,
#        #with_participant_phone = False,
        )
    location = factory.fuzzy.FuzzyChoice(['unspecified','home','work','temporary'])
    primary = factory.fuzzy.FuzzyChoice([True,False])
    number = factory.fuzzy.FuzzyText(length=30)
    type = factory.fuzzy.FuzzyChoice(['Mobile','Landline','Fax'])
    
    class Meta:
        model = ParticipantPhone
    
class ParticipantEmailFactory(DjangoModelFactory):
    participant = factory.SubFactory(
        ParticipantFactory,
        
        #with_participant_email = False,
        )
    location = factory.fuzzy.FuzzyChoice(['unspecified','home','work','temporary'])
    primary = factory.fuzzy.FuzzyChoice([True,False])
    email = factory.fuzzy.FuzzyText(length=30)
    
    class Meta:
        model = ParticipantEmail
    
class ParticipantAddressFactory(DjangoModelFactory):
    participant = factory.SubFactory(ParticipantFactory)
    location = factory.fuzzy.FuzzyChoice(['unspecified','home','work','temporary'])
    primary = factory.fuzzy.FuzzyChoice([True,False])
    address_line_1 = factory.fuzzy.FuzzyText(length=30)
    address_line_2 = factory.fuzzy.FuzzyText(length=30)
    address_line_3 = factory.fuzzy.FuzzyText(length=30)
    address_line_4 = factory.fuzzy.FuzzyText(length=30)
    address_line_5 = factory.fuzzy.FuzzyText(length=30)
    address_line_6 = factory.fuzzy.FuzzyText(length=30)
    country = factory.fuzzy.FuzzyText(length=2) # this is a CountryField - # factory.fuzzy.FuzzyText(length=30)
    postcode = factory.fuzzy.FuzzyText(length=10)
    carehome = factory.fuzzy.FuzzyChoice([True,False])
    
    class Meta:
        model = ParticipantAddress

#class ParticipantDetailFactory(DjangoModelFactory):
#    '''this will be a factory for testing combi views/forms
#       not working yet
#    '''
#    class Meta:
#        model = Participant
#        
#    participant = ParticipantFactory()
#    
#    participant_phone = factory.SubFactory(
#        'white2_project.white2.tests.factories.ParticipantPhoneFactory',
#        participant = factory.SelfAttribute('..participant')
#    )

@pytest.fixture
def fred_bloggs():
    return ParticipantFactory(study_no=10001,check_letter='X',surname = 'Bloggs', title = 'Lord', initials = 'F')

@pytest.fixture
def participant():
    return ParticipantFactory()

phone_number = '0201 1234 5678'
@pytest.fixture
def participant_phone(participant):
    return ParticipantPhoneFactory(participant=participant, number=phone_number)


email_address = 'any_old_email@address.com'
@pytest.fixture
def participant_email(participant):
    return ParticipantEmailFactory(participant = participant,email = email_address)

address = ['PoshHouseName','1, Road Street','Village Town','Countyshire','a5','a6','PC0 0DE','GB']
@pytest.fixture
def participant_address(participant):
    return ParticipantAddressFactory(
        participant = participant,
        address_line_1=address[0],
        address_line_2=address[1],
        address_line_3=address[2],
        address_line_4=address[3],
        address_line_5=address[4],
        address_line_6=address[5],
        postcode=address[6],
        country=address[7],
        )