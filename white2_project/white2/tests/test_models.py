import pytest
import random

from .factories import (
    ParticipantFactory,
    ParticipantPhoneFactory,
    ParticipantEmailFactory,
    ParticipantAddressFactory,
    participant,
    fred_bloggs,
    participant_phone,
    participant_email,
    participant_address,
    address,
    email_address,
    phone_number,
    
)

from ..models import Participant

pytestmark = pytest.mark.django_db

    
def test_uppercasecharfield_pre_save(participant):
    # testing for the one place this field is used
    if getattr(participant,'check_letter',None):
        assert participant.check_letter == participant.check_letter.upper()
    else: 
        #should be Null - not sure why this isn't working
        assert participant.checkletter == None
        
class TestParticipant:
    def test_title_inits_surname(self,fred_bloggs):
        assert fred_bloggs.title_inits_surname() == 'Lord F Bloggs'

    def test__str__(self,fred_bloggs):
        assert fred_bloggs.__str__() == '10001X, Lord F Bloggs'

    def test_get_absolute_url(self,participant):
        url = f"/white2/{participant.slug}/"
        assert participant.get_absolute_url() == url
    
#def test_get_all_instance_fields():
#    #need an instance
#    participant = ParticipantFactory()
#    #check that all fields in the model are listed in the instance
#    i_fields = participant.get_all_fields()
#    m_fields = Participant._meta.get_fields()

class TestParticipantPhone:    
    def test__str__(self,participant_phone):
        assert participant_phone.__str__() == phone_number
    
class TestParticipantEmail:
    def test_participat_email__str__(self,participant_email):
        assert participant_email.__str__() == email_address
    
class TestParticipantAddress:
    def test_participant_address__str__(self,participant_address):
        assert participant_address.__str__() == \
            address[0] + ', ' + address[1] + ', ' + address[2] + ', ' + address[3] + ', ' + \
                address[4] + ', ' + address[5] + ', ' + address[6] + ', ' + 'United Kingdom'
    
