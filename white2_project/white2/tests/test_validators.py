from datetime import datetime
import pytest
from django.core.exceptions import ValidationError
from datetime import date

from ..validators import date_of_birth_in_range

pytestmark = pytest.mark.django_db

dob = [date(1930,4,1), date(1952,11,28),]

def test_date_of_birth_in_range():
    pytest.raises(ValidationError,date_of_birth_in_range,dob[0])
    try:
        date_of_birth_in_range(dob[1])
    except ValidationError as exc:
        assert False, f'date_of_birth_in_range raised an exception {exc}'
        