import pytest
from django.urls import reverse,resolve

from white2_project import white2

from .factories import ParticipantFactory

pytestmark = pytest.mark.django_db

@pytest.fixture
def participant():
    return ParticipantFactory()


def test_list_reverse():
    """white2:participants should reverse to /white2/."""
    assert reverse('white2:participants') == '/white2/'

def test_list_resolve():
    """/white2/ should resolve to white2:participants."""
    assert resolve('/white2/').view_name == 'white2:participants'
    
def test_detail_reverse(participant):
    """white2:participant reverses to /white2/participantslug/
    """
    url = reverse('white2:participant',kwargs={'slug': participant.slug})
    assert url == f'/white2/{participant.slug}/'
    
def test_detail_resolve(participant):
    """/white2/participantslug/ should resolve to white2:participant"""
    url = f'/white2/{participant.slug}/'
    assert resolve(url).view_name == 'white2:participant'
    
#2 more to test
#    path('<slug:slug>/update/', view=ParticipantUpdateView.as_view(), name="participant-update"),
#    path('<slug:slug>/contact/', view=ParticipantContactUpdateView.as_view(), name="participant-contact-update"),
