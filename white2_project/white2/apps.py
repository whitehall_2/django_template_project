from django.apps import AppConfig


class White2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'white2_project.white2'
