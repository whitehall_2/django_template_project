from audioop import add
from multiprocessing import context
from django.forms import inlineformset_factory
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views.generic import ListView, DetailView, UpdateView,FormView
from django.views.generic.edit import FormMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render,redirect
from django.db import transaction
from django.forms.models import modelformset_factory
from django.template import RequestContext

from django.urls import reverse
import re

from .models import  Participant, ParticipantPhone, ParticipantEmail, ParticipantAddress
from .forms import ParticipantAddressForm, ParticipantEmailForm, ParticipantPhoneForm, ParticipantForm, PhoneFormset,EmailFormset,AddressFormset#, ParticipantStatusForm,ParticipantNonStatusForm

class ParticipantListView(ListView):
    '''
    render list of participants
    '''
    model = Participant

#    def head(self, *args, **kwargs):
#        '''
#        override head() to show midified timestamp in the HEAD
#        '''
#        last_participant = self.get_queryset().latest('datetime_updated')
#        response = HttpResponse(
#            # RFC 1123 date format.
#            headers={'Last-Modified': last_participant.datetime_updated.strftime('%a, %d %b %Y %H:%M:%S GMT')},
#        )
#        return response

    
class ParticipantDetailView(LoginRequiredMixin, DetailView):

    model = Participant
    
    def get_context_data(self, **kwargs):
        '''
        Return: participant context plus all the participant's contact details
        '''
        context = super().get_context_data(**kwargs)
        try:
            context["phone"] = ParticipantPhone.objects.filter(participant_id=context['participant'].id)
        except AttributeError:
            context["phone"] = None
        try:
            context["email"] = ParticipantEmail.objects.filter(participant_id=context['participant'].id)
        except AttributeError:
            context["email"] = None
        try:
            context["address"] = ParticipantAddress.objects.filter(participant_id=context['participant'].id)
        except AttributeError: 
            context["address"] = None
        return context

class PhoneUpdateView(LoginRequiredMixin, FormMixin, DetailView):
    template_name = 'white2\participant_phone_form.html'
    model = ParticipantPhone
    form_class = ParticipantPhoneForm
    fields = ['number','type','location','primary']
    slug_field = "participant_id"
    slug_url_kwarg = "participant_id"
    lookup_url_kwarg = 'participant_id'
    success_url = 'white2:participants'
    
    def get_context_data(self, **kwargs):
        #error on next line:
        #error: Argument "formset" to "inlineformset_factory" has incompatible type "Type[ParticipantPhoneForm]"; expected "Type[BaseInlineFormSet]"
        #TODO deal with it
        PhoneFormSet = inlineformset_factory(
            Participant,
            ParticipantPhone,
            fields=('number','type','location','primary'),
            formset=ParticipantPhoneForm,
            fk_name='participant'
            )
        context = super(PhoneUpdateView,self).get_context_data(**kwargs)
        context['phone_form'] = PhoneFormSet(instance=context['Participant'])
        
        return context
    
class ParticipantUpdateView(LoginRequiredMixin,UpdateView):
    template_name = 'white2/participant_form.html'
    form_class = ParticipantForm
    model = Participant 
    
    def get_success_url(self):
        #return self.request.participant.get_absolute_url()
        return reverse('white2:participant',kwargs={'slug':self.kwargs['slug']})

class ParticipantContactUpdateView(LoginRequiredMixin,UpdateView):
    template_name = 'white2/participant_contact_form.html' 
    #form_class = ParticipantHeaderForm
    model = Participant
    #don't naad to update any fields, but they won't be available as form fields - or should I restrict this???
    fields="__all__"
        
    def get_context_data(self, **kwargs):
        context = super(ParticipantContactUpdateView,self).get_context_data(**kwargs)
        if self.request.POST:
            context['phone_formset']=PhoneFormset(self.request.POST,instance=self.object,prefix="phone")
            context['phone_formset'].full_clean()
            context['email_formset']=EmailFormset(self.request.POST,instance=self.object,prefix="email")
            context['email_formset'].full_clean()
            context['address_formset']=AddressFormset(self.request.POST,instance=self.object,prefix="address")
            context['address_formset'].full_clean()
        else:
            context['phone_formset']=PhoneFormset(instance=self.object)
            context['email_formset']=EmailFormset(instance=self.object)
            context['address_formset']=AddressFormset(instance=self.object)
        return context
    
    def post(self,request,*args,**kwargs):
        self.object = self.get_object()
        form = self.get_form()
        phone_formset = PhoneFormset(self.request.POST,prefix="phone")
        email_formset = EmailFormset(self.request.POST,prefix="email")
        address_formset = AddressFormset(self.request.POST,prefix="address")
        #phone_formset=context['phone_formset']
        #email_formset=context['email_formset']
        #address_formset=context['address_formset']
        if form.is_valid() and phone_formset.is_valid() and email_formset.is_valid() and address_formset.is_valid():
            return self.form_valid(form,phone_formset,email_formset,address_formset)
        else:
            return self.form_invalid(form,phone_formset,email_formset,address_formset)
    
    def form_valid(self,form,phone_formset,email_formset,address_formset):
        self.object=form.save()
        phone_formset.instance = self.object
        phone_formset.save()
        email_formset.instance = self.object
        email_formset.save()
        address_formset.instance = self.object
        address_formset.save()
        return HttpResponseRedirect(self.get_success_url())
        
    def form_invalid(self,form,phone_formset,email_formset,address_formset):
        return self.render_to_response(self.get_context_data(form=form,phone_formset=phone_formset,email_formset=email_formset,address_formset=address_formset))        
            
    def get_success_url(self):
        #return self.request.participant.get_absolute_url()
        return reverse('white2:participant',kwargs={'slug':self.kwargs['slug']})


        
        