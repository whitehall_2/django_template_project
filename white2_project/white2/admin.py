from django.contrib import admin

from .models import ParticipantAddress, ParticipantEmail, Participant, ParticipantPhone

class ParticipantPhoneInline(admin.TabularInline):
    '''
    temporary class for development
    '''
    model = ParticipantPhone
    extra = 0

class ParticipantEmailInline(admin.TabularInline):
    '''
    temporary class for development
    '''
    model = ParticipantEmail
    extra = 0

class ParticipantAddressInline(admin.StackedInline):
    '''
    temporary class for development
    '''
    model = ParticipantAddress
    extra = 0

@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin):
    '''
    temporary class for development
    '''
    
    inlines = [
        ParticipantPhoneInline,
        ParticipantEmailInline,
        ParticipantAddressInline,
    ]
